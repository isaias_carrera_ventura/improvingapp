package com.improving.styles.component.movierecyclerview.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.improving.styles.component.movierecyclerview.adapter.viewholder.MovieViewHolder
import com.improving.styles.component.movierecyclerview.presenter.MoviePresenter
import com.improving.styles.R
import com.improving.styles.component.movierecyclerview.model.MovieItem

internal class MovieAdapter(
    private val presenter: MoviePresenter
) : RecyclerView.Adapter<MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        presenter.onBindViewHolder(holder, position)
    }

    override fun getItemCount(): Int = presenter.getListSize()

    fun updateData(list: List<MovieItem>) {
        val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                oldItemPosition == newItemPosition

            override fun getOldListSize(): Int = presenter.list.size

            override fun getNewListSize(): Int = list.size

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = false
        })
        presenter.list = list.toMutableList()
        result.dispatchUpdatesTo(this)
    }
}