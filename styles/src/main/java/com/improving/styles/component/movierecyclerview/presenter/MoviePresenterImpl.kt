package com.improving.styles.component.movierecyclerview.presenter

import com.improving.styles.component.movierecyclerview.adapter.viewholder.view.MovieView
import com.improving.styles.component.movierecyclerview.model.MovieItem

internal class MoviePresenterImpl : MoviePresenter {

    override var list: List<MovieItem> = emptyList()

    override fun onBindViewHolder(holder: MovieView, position: Int) {
        if (position < list.size) {
            val item = list[position]
            holder.setItemInView(item)
        }
    }

    override fun getListSize(): Int = list.size

}