package com.improving.styles.component.movierecyclerview.adapter.viewholder.view

import com.improving.styles.component.movierecyclerview.model.MovieItem

internal interface MovieView {
    fun setItemInView(item: MovieItem)
}