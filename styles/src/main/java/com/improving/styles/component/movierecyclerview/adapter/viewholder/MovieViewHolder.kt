package com.improving.styles.component.movierecyclerview.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.improving.styles.component.movierecyclerview.adapter.viewholder.view.MovieView
import com.improving.styles.component.movierecyclerview.model.MovieItem
import com.improving.styles.databinding.MovieItemBinding
import com.improving.utils.loadImageUrl
import kotlinx.android.extensions.LayoutContainer

internal class MovieViewHolder(
    view: View
) : RecyclerView.ViewHolder(view), LayoutContainer, MovieView {

    private val binding = MovieItemBinding.bind(view)
    override val containerView: View = binding.root

    override fun setItemInView(item: MovieItem) {
        binding.movieImageView.loadImageUrl(item.imageUrl)
    }
}