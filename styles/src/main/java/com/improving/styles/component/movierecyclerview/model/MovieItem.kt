package com.improving.styles.component.movierecyclerview.model

data class MovieItem(val imageUrl: String)