package com.improving.styles.component.movierecyclerview.view

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.improving.styles.component.movierecyclerview.adapter.MovieAdapter
import com.improving.styles.component.movierecyclerview.model.MovieItem
import com.improving.styles.component.movierecyclerview.presenter.MoviePresenter
import com.improving.styles.component.movierecyclerview.presenter.MoviePresenterImpl

class MovieRecyclerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RecyclerView(context, attrs, defStyle) {

    private val presenter: MoviePresenter
    private val adapterAction: MovieAdapter

    init {
        layoutManager = GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false)
        presenter = MoviePresenterImpl()
        adapterAction = MovieAdapter(presenter)
        this.adapter = adapterAction
        itemAnimator = null
    }

    fun setData(list: List<MovieItem>) {
        adapterAction.updateData(list)
    }
}