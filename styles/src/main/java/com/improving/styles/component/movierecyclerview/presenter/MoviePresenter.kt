package com.improving.styles.component.movierecyclerview.presenter

import com.improving.styles.component.movierecyclerview.adapter.viewholder.view.MovieView
import com.improving.styles.component.movierecyclerview.model.MovieItem

internal interface MoviePresenter {
    var list: List<MovieItem>
    fun onBindViewHolder(holder: MovieView, position: Int)
    fun getListSize(): Int
}