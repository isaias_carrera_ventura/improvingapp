package com.improving.utils

import android.view.View
import android.widget.ImageView
import com.squareup.picasso.Picasso

fun View.makeItInvisible() {
    this.visibility = View.INVISIBLE
}

fun View.makeItVisible() {
    this.visibility = View.VISIBLE
}

fun ImageView.loadImageUrl(imageUrl: String) {
    if (imageUrl.isNotEmpty()) {
        Picasso.get()
            .load(imageUrl)
            .into(this)
    }
}