package com.gbm.improvingchallenge.feature.movies.domain.mapper

import com.improving.data.local.entity.MovieEntity
import com.improving.styles.component.movierecyclerview.model.MovieItem
import javax.inject.Inject

class MovieListMapper @Inject constructor() {

    fun createMovieItemListFromMovieEntity(entityList: List<MovieEntity>): List<MovieItem> {
        return entityList.map {
            MovieItem(it.imageUrl)
        }
    }
}