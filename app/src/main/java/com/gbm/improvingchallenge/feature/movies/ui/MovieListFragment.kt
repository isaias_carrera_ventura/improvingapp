package com.gbm.improvingchallenge.feature.movies.ui

import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.gbm.improvingchallenge.R
import com.gbm.improvingchallenge.databinding.FragmentMovieListBinding
import com.gbm.improvingchallenge.feature.movies.viewmodel.MovieListViewState
import com.gbm.improvingchallenge.feature.movies.viewmodel.MovieViewModel
import com.improving.styles.lifecycle.ImprovingBaseFragment
import com.improving.utils.makeItInvisible
import com.improving.utils.makeItVisible
import com.improving.utils.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_movie_list.*
import javax.inject.Inject


class MovieListFragment : ImprovingBaseFragment() {

    companion object {
        fun newInstance() = MovieListFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: MovieViewModel
    private lateinit var binding: FragmentMovieListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViewModel()
    }

    private fun setupViewModel() {
        activity?.apply {
            viewModel = ViewModelProvider(this, viewModelFactory)
                .get(MovieViewModel::class.java)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMovieListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startObserving()
        viewModel.getTopRatedMovieList()
    }

    private fun startObserving() {
        viewModel.state.observe(viewLifecycleOwner, Observer(::onDataReceived))
    }

    private fun onDataReceived(state: MovieListViewState) {
        when (state) {
            MovieListViewState.OnLoading -> showProgressBar()
            MovieListViewState.OnDoneLoading -> hideProgressBar()
            MovieListViewState.OnListEmptyData -> showEmptyView()
            is MovieListViewState.OnListDataReady -> binding.movieRecyclerView.setData(state.dataList)
        }
    }

    private fun showEmptyView() {
        binding.movieRecyclerView.makeItInvisible()
    }

    private fun hideProgressBar() {
        movieLisProgressBar.makeItInvisible()
    }

    private fun showProgressBar() {
        movieLisProgressBar.makeItVisible()
    }

    override fun provideLayoutId(): Int = R.layout.fragment_movie_list

    override fun hasMenu(): Boolean = true

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.movie_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.topRated -> {
                viewModel.getTopRatedMovieList()
                true
            }
            R.id.popular -> {
                viewModel.getMostPopularMovieList()
                true
            }
            else -> false
        }
    }
}