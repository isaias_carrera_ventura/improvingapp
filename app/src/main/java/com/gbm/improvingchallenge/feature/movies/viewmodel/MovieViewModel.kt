package com.gbm.improvingchallenge.feature.movies.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gbm.improvingchallenge.feature.movies.domain.interactor.MovieListInteractor
import com.improving.styles.component.movierecyclerview.model.MovieItem
import kotlinx.coroutines.launch
import javax.inject.Inject

class MovieViewModel @Inject constructor(private val movieInteractor: MovieListInteractor) :
    ViewModel() {

    val state: MutableLiveData<MovieListViewState> = MutableLiveData()

    private fun setState(state: MovieListViewState) {
        this.state.value = state
    }

    fun getTopRatedMovieList() {
        getMovieListByInteractorCall {
            movieInteractor.getTopRatedMovieList()
        }
    }

    fun getMostPopularMovieList() {
        getMovieListByInteractorCall {
            movieInteractor.getMostPopularMovieList()
        }
    }

    private fun getMovieListByInteractorCall(interactorCall: suspend () -> List<MovieItem>) {
        viewModelScope.launch {
            setState(MovieListViewState.OnLoading)
            setStateByList(interactorCall())
            setState(MovieListViewState.OnDoneLoading)
        }
    }

    private fun setStateByList(topRatedMovieList: List<MovieItem>) {
        if (topRatedMovieList.isEmpty()) {
            setState(MovieListViewState.OnListEmptyData)
        } else {
            setState(MovieListViewState.OnListDataReady(topRatedMovieList))
        }
    }

}