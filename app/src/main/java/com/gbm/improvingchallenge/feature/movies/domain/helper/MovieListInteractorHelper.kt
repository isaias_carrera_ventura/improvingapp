package com.gbm.improvingchallenge.feature.movies.domain.helper

import com.gbm.improvingchallenge.feature.movies.domain.mapper.MovieListMapper
import com.improving.data.local.entity.MovieEntity
import com.improving.manager.model.CoreResult
import com.improving.styles.component.movierecyclerview.model.MovieItem
import javax.inject.Inject

class MovieListInteractorHelper @Inject constructor(
    private val mapper: MovieListMapper
) {

    fun createTopRatedMovieList(coreResult: CoreResult<List<MovieEntity>>): List<MovieItem> {
        return coreResult.entity?.let {
            mapper.createMovieItemListFromMovieEntity(it)
        } ?: emptyList()
    }

}