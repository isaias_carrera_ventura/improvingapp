package com.gbm.improvingchallenge.feature.movies.ui

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.gbm.improvingchallenge.feature.movies.viewmodel.MovieViewModel
import com.improving.styles.lifecycle.ImprovingBaseActivity

class MovieActivity : ImprovingBaseActivity() {

    private lateinit var viewModel: MovieViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViewModel()
        initFlow()
    }

    private fun setupViewModel() {
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(MovieViewModel::class.java)
    }

    private fun initFlow() {
        addFragment(MovieListFragment.newInstance())
    }
}