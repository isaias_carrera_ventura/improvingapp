package com.gbm.improvingchallenge.feature.movies.viewmodel

import com.improving.styles.component.movierecyclerview.model.MovieItem

sealed class MovieListViewState {

    object OnLoading : MovieListViewState()
    object OnDoneLoading : MovieListViewState()
    object OnListEmptyData : MovieListViewState()
    data class OnListDataReady(val dataList: List<MovieItem>) : MovieListViewState()

}
