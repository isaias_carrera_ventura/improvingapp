package com.gbm.improvingchallenge.feature.movies.domain.interactor

import com.gbm.improvingchallenge.feature.movies.domain.helper.MovieListInteractorHelper
import com.improving.manager.repository.abstraction.MovieRepository
import com.improving.styles.component.movierecyclerview.model.MovieItem
import javax.inject.Inject

class MovieListInteractor @Inject constructor(
    private val repository: MovieRepository,
    private val helper: MovieListInteractorHelper
) {
    suspend fun getTopRatedMovieList(): List<MovieItem> {
        val coreResult = repository.getTopRatedMovieList()
        return helper.createTopRatedMovieList(coreResult)
    }

    suspend fun getMostPopularMovieList(): List<MovieItem> {
        val coreResult = repository.getPopularMovieList()
        return helper.createTopRatedMovieList(coreResult)
    }
}