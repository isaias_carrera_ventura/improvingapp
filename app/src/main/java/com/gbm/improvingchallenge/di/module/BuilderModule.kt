package com.gbm.improvingchallenge.di.module

import com.gbm.improvingchallenge.feature.movies.ui.MovieActivity
import com.gbm.improvingchallenge.feature.movies.ui.MovieListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [DomainModule::class, ViewModelModule::class])
abstract class BuilderModule {

    @ContributesAndroidInjector
    abstract fun bindMovieActivity(): MovieActivity

    @ContributesAndroidInjector
    abstract fun bindMovieFragment(): MovieListFragment
}