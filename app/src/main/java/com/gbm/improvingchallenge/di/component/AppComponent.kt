package com.gbm.improvingchallenge.di.component

import android.app.Application
import android.content.Context
import com.gbm.improvingchallenge.ImprovingApplication
import com.gbm.improvingchallenge.di.module.BuilderModule
import com.improving.di.CoreModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,

        BuilderModule::class,
        CoreModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        @BindsInstance
        fun context(context: Context): Builder

        fun build(): AppComponent
    }

    fun inject(coreApplication: ImprovingApplication)
}