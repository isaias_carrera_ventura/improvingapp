package com.gbm.improvingchallenge.di.module

import androidx.lifecycle.ViewModel
import com.gbm.improvingchallenge.feature.movies.domain.interactor.MovieListInteractor
import com.gbm.improvingchallenge.feature.movies.viewmodel.MovieViewModel
import com.improving.manager.repository.abstraction.MovieRepository
import com.improving.utils.di.module.ViewModelKey
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap


@Module
class ViewModelModule {

    @Provides
    @IntoMap
    @ViewModelKey(MovieViewModel::class)
    fun provideIPCViewModel(interactor: MovieListInteractor): ViewModel {
        return MovieViewModel(interactor)
    }
}