package com.gbm.improvingchallenge.di.module

import com.gbm.improvingchallenge.feature.movies.domain.helper.MovieListInteractorHelper
import com.gbm.improvingchallenge.feature.movies.domain.interactor.MovieListInteractor
import com.gbm.improvingchallenge.feature.movies.domain.mapper.MovieListMapper
import com.improving.manager.repository.abstraction.MovieRepository
import dagger.Module
import dagger.Provides

@Module
class DomainModule {

    @Provides
    fun provideMovieInteractor(
        helper: MovieListInteractorHelper,
        repository: MovieRepository
    ): MovieListInteractor =
        MovieListInteractor(repository, helper)

    @Provides
    fun provideMovieInteractorHelper(
        movieListMapper: MovieListMapper
    ): MovieListInteractorHelper = MovieListInteractorHelper(movieListMapper)

    @Provides
    fun provideMovieListMapper(): MovieListMapper = MovieListMapper()

}