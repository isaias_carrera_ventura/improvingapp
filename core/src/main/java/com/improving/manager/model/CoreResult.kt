package com.improving.manager.model

import com.improving.manager.exception.NoInternetConnectionException
import java.lang.Exception
import kotlin.jvm.Throws

data class CoreResult<Result>(
    var exception: Exception? = null,
    var entity: Result? = null
) {
    fun isNetworkAvailable() = exception !is NoInternetConnectionException
    @Throws(Exception::class)
    fun getEntityOrThrowException() = entity ?: throw exception ?: Exception()
}