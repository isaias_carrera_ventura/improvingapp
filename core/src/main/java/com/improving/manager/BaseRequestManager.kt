package com.improving.manager

import com.improving.manager.exception.InvalidDtoException
import com.improving.manager.exception.NoInternetConnectionException
import com.improving.manager.model.CoreResult
import com.improving.utils.NetworkUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal abstract class BaseRequestManager<Result, DTO>(
    private val networkUtil: NetworkUtil
) {

    fun isNetworkConnected() = networkUtil.isNetworkAvailable()

    suspend fun getData(): CoreResult<Result> =
        if (isNetworkConnected())
            makeRequest()
        else
            CoreResult(NoInternetConnectionException(), onApiCallError())

    suspend fun makeRequest(): CoreResult<Result> =
        try {
            val dto: DTO = makeApiCall()
            onApiCallSuccess(dto)
        } catch (t: Throwable) {
            CoreResult(InvalidDtoException(), onApiCallError())
        }

    suspend fun onApiCallError() = withContext(Dispatchers.Main) {
        try {
            performActionOnError()
        } catch (t: Throwable) {
            null
        }
    }

    suspend fun onApiCallSuccess(dto: DTO): CoreResult<Result> = withContext(Dispatchers.Main) {
        val coreResult = CoreResult<Result>()
        try {
            coreResult.entity = performActionOnSuccess(dto)
        } catch (t: Throwable) {
            coreResult.exception = Exception()
        }
        coreResult
    }

    @Throws(Throwable::class)
    abstract suspend fun makeApiCall(): DTO

    @Throws(Throwable::class)
    abstract suspend fun performActionOnError(): Result

    @Throws(Throwable::class)
    abstract suspend fun performActionOnSuccess(dto: DTO): Result
}