package com.improving.manager.exception

import java.lang.Exception

class NoInternetConnectionException : Exception()