package com.improving.manager.exception

import java.lang.Exception

class InvalidDtoException : Exception()