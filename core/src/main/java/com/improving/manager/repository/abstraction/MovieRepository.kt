package com.improving.manager.repository.abstraction

import com.improving.data.local.entity.MovieEntity
import com.improving.manager.model.CoreResult

interface MovieRepository {
    suspend fun getPopularMovieList(): CoreResult<List<MovieEntity>>
    suspend fun getTopRatedMovieList(): CoreResult<List<MovieEntity>>
}