package com.improving.manager.repository.implementation.helper

import android.util.Log
import com.improving.data.local.entity.MovieEntity
import com.improving.data.remote.NetworkService
import com.improving.data.remote.model.MovieListDTO
import com.improving.manager.repository.mapper.MovieMapper
import javax.inject.Inject

internal class MovieRepositoryHelper @Inject constructor(
    private val networkService: NetworkService,
    private val mapper: MovieMapper
) {

    suspend fun getPopularMovieService(): MovieListDTO = networkService.getPopularMovieList()

    fun createEntityListFromMovieListDto(dto: MovieListDTO): List<MovieEntity> {
        return mapper.mapMovieListDtoToMovieListEntity(dto)
    }

    suspend fun getTopRatedMovieService(): MovieListDTO = networkService.getTopRatedMovieList()
}