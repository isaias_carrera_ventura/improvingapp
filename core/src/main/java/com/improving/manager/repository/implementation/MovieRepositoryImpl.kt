package com.improving.manager.repository.implementation

import com.improving.data.local.entity.MovieEntity
import com.improving.data.remote.model.MovieListDTO
import com.improving.manager.BaseRequestManager
import com.improving.manager.model.CoreResult
import com.improving.manager.repository.abstraction.MovieRepository
import com.improving.manager.repository.implementation.helper.MovieRepositoryHelper
import com.improving.utils.NetworkUtil
import javax.inject.Inject

internal class MovieRepositoryImpl @Inject constructor(
    private val networkUtil: NetworkUtil,
    private val helper: MovieRepositoryHelper
) : MovieRepository {

    override suspend fun getPopularMovieList(): CoreResult<List<MovieEntity>> {
        val manager = object : BaseRequestManager<List<MovieEntity>, MovieListDTO>(networkUtil) {

            override suspend fun makeApiCall(): MovieListDTO = helper.getPopularMovieService()

            override suspend fun performActionOnError(): List<MovieEntity> = emptyList()

            override suspend fun performActionOnSuccess(dto: MovieListDTO): List<MovieEntity> =
                helper.createEntityListFromMovieListDto(dto)

        }
        return manager.getData()
    }

    override suspend fun getTopRatedMovieList(): CoreResult<List<MovieEntity>> {

        val manager = object : BaseRequestManager<List<MovieEntity>, MovieListDTO>(networkUtil) {

            override suspend fun makeApiCall(): MovieListDTO = helper.getTopRatedMovieService()

            override suspend fun performActionOnError(): List<MovieEntity> = emptyList()

            override suspend fun performActionOnSuccess(dto: MovieListDTO): List<MovieEntity> =
                helper.createEntityListFromMovieListDto(dto)

        }
        return manager.getData()
    }
}