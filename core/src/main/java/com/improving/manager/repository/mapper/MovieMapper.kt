package com.improving.manager.repository.mapper

import com.improving.data.local.entity.MovieEntity
import com.improving.data.remote.model.MovieListDTO
import javax.inject.Inject

internal class MovieMapper @Inject constructor() {

    companion object {
        const val BASE_IMAGE_URL = "https://image.tmdb.org/t/p/w300"
    }

    fun mapMovieListDtoToMovieListEntity(dto: MovieListDTO): List<MovieEntity> {
        return dto.results.map {
            MovieEntity(
                it.id,
                it.original_title,
                it.overview,
                it.vote_average,
                it.release_date,
                createImageUrlFromMovieDto(it.poster_path)
            )
        }
    }

    private fun createImageUrlFromMovieDto(posterPath: String): String {
        return "$BASE_IMAGE_URL${posterPath}"
    }

}