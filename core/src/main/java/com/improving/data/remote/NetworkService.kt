package com.improving.data.remote

import com.improving.data.remote.model.MovieListDTO
import retrofit2.http.GET

internal interface NetworkService {
    @GET("/3/movie/popular")
    suspend fun getPopularMovieList(): MovieListDTO

    @GET("/3/movie/top_rated")
    suspend fun getTopRatedMovieList(): MovieListDTO
}