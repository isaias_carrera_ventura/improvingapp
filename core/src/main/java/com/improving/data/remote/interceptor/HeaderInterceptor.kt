package com.improving.data.remote.interceptor

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject

internal class HeaderInterceptor @Inject constructor(private val apiKey: String) : Interceptor {

    companion object {
        const val API_KEY = "api_key"
    }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val url = createHttpUrl(original)
        val request = createRequest(original, url)
        return chain.proceed(request)
    }

    private fun createHttpUrl(original: Request): HttpUrl {
        return original.url.newBuilder().addQueryParameter(API_KEY, apiKey).build()
    }

    private fun createRequest(original: Request, url: HttpUrl): Request {
        return original.newBuilder().url(url).build()
    }
}