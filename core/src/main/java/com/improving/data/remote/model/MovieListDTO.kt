package com.improving.data.remote.model

import com.google.gson.annotations.SerializedName

internal data class MovieListDTO(
    @SerializedName("page") val page: Int,
    @SerializedName("results") val results: List<MovieItemDTO>,
    @SerializedName("total_pages") val total_pages: Int,
    @SerializedName("total_results") val total_results: Int
)