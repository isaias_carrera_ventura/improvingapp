package com.improving.data.local.entity

data class MovieEntity(
    val id: Int,
    val title: String,
    val synopsis: String,
    val rating: Double,
    val releaseDate: String,
    val imageUrl: String
)