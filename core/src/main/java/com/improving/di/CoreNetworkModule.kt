package com.improving.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.improving.core.BuildConfig
import com.improving.data.remote.NetworkService
import com.improving.data.remote.interceptor.HeaderInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
internal class CoreNetworkModule {

    @Provides
    @Singleton
    fun providesGson(): Gson {
        return GsonBuilder().create()
    }

    @Provides
    @Singleton
    fun provideHeaderInterceptor(@Named("API_KEY") apiKey: String): HeaderInterceptor {
        return HeaderInterceptor(apiKey)
    }

    @Provides
    @Singleton
    fun providesOkHttpClient(headerInterceptor: HeaderInterceptor): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor(headerInterceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideNetworkService(
        @Named("BASE_URL") baseUrl: String,
        gson: Gson,
        okHttpClient: OkHttpClient
    ): NetworkService {
        return Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(baseUrl)
            .build()
            .create(NetworkService::class.java)
    }

    @Provides
    @Singleton
    @Named("API_KEY")
    fun provideApiKey(): String {
        return BuildConfig.API_KEY
    }

    @Provides
    @Singleton
    @Named("BASE_URL")
    fun provideURL(): String {
        return BuildConfig.BASE_URL
    }
}