package com.improving.di

import com.improving.data.remote.NetworkService
import com.improving.manager.repository.abstraction.MovieRepository
import com.improving.manager.repository.implementation.MovieRepositoryImpl
import com.improving.manager.repository.implementation.helper.MovieRepositoryHelper
import com.improving.manager.repository.mapper.MovieMapper
import com.improving.utils.NetworkUtil
import com.improving.utils.di.module.ViewModelFactoryModule
import dagger.Module
import dagger.Provides


@Module(
    includes = [
        CoreUtilsModule::class,
        CoreMapperModule::class,
        ViewModelFactoryModule::class,
        CoreNetworkModule::class
    ]
)
class CoreModule {

    @Provides
    internal fun provideMovieRepository(
        networkUtil: NetworkUtil,
        helper: MovieRepositoryHelper
    ): MovieRepository =
        MovieRepositoryImpl(networkUtil, helper)

    @Provides
    internal fun provideMovieHelper(
        networkService: NetworkService,
        mapper: MovieMapper
    ): MovieRepositoryHelper = MovieRepositoryHelper(networkService, mapper)


}