package com.improving.di

import com.improving.manager.repository.mapper.MovieMapper
import dagger.Module
import dagger.Provides

@Module
internal class CoreMapperModule {
    @Provides
    fun provideMovieMapper(): MovieMapper = MovieMapper()
}