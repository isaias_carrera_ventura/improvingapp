package com.improving.di

import android.content.Context
import com.improving.utils.NetworkUtil
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
internal class CoreUtilsModule {

    @Provides
    @Singleton
    fun provideNetworkUtil(
        context: Context
    ): NetworkUtil = NetworkUtil(context)

}